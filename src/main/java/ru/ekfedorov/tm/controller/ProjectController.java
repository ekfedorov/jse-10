package ru.ekfedorov.tm.controller;

import ru.ekfedorov.tm.api.IProjectController;
import ru.ekfedorov.tm.api.IProjectService;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    public final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        if (projects.size() != 0) {
            for (final Project project : projects) {
                System.out.println(index + ". " + project);
                index++;
            }
        } else System.out.println("--- There are no projects ---");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        String name = TerminalUtil.nextLine();
        if ("".equals(name)) name = "project";
        System.out.println("ENTER DESCRIPTION:");
        String description = TerminalUtil.nextLine();
        if ("".equals(description)) description = "without description";
        final Project project = projectService.add(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println();
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR]");
        projectService.clear();
        System.out.println("--- successfully cleared ---\n");
    }

}
