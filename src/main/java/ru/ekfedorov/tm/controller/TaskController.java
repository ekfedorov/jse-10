package ru.ekfedorov.tm.controller;

import ru.ekfedorov.tm.api.ITaskController;
import ru.ekfedorov.tm.api.ITaskService;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    public final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        if (tasks.size() != 0) {
            for (final Task task : tasks) {
                System.out.println(index + ". " + task);
                index++;
            }
        } else System.out.println("--- There are no tasks ---");
        System.out.println();
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        String name = TerminalUtil.nextLine();
        if ("".equals(name)) name = "task";
        System.out.println("ENTER DESCRIPTION:");
        String description = TerminalUtil.nextLine();
        if ("".equals(description)) description = "without description";
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println();
    }

    @Override
    public void clear() {
        System.out.println("[CLEAR]");
        taskService.clear();
        System.out.println("--- successfully cleared ---\n");
    }

}
