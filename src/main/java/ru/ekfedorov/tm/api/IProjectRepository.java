package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void add(Project project);

    void remove(Project project);

    void clear();

}
