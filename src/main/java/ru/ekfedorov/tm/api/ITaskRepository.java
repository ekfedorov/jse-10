package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void add(Task task);

    void remove(Task task);

    void clear();

}
