package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    List<Project> findAll();

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();
}
